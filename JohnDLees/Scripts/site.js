﻿(function (johndlees, $, undefined) {
	$(document).ready(function () {
		PDFObject.embed("/Content/JohnDLeesCV.pdf#toolbar=0&navpanes=0&scrollbar=0", "#pdfRenderer");
	});

	$("button.download-cv").click(function () {

		window.location.href = $("#DownloadCvLocation").val();
		
	});

	$(".dropdown").click(function () {

		var $header = $(this);
		var $content = $header.next();

		//open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
		$content.slideToggle(500, function () {
			if (!$content.is(":visible")) {
				$header.append("<span class='click-to-expand'> - click to expand</span>");
			}     
		});

		if ($content.is(":visible")) {
			$header.find("span").remove();
		}
	});

	// Adds the expand span on page load
	$(".dropdown").each(function() {
		var $header = $(this);
		var $content = $header.next();

		if ($content.is(":visible")) {
			$header.find("span").remove();
		} else {
			$header.append("<span class='click-to-expand'> - click to expand</span>");
		}
	});

}(window.johndlees = window.johndlees || {}, jQuery));

