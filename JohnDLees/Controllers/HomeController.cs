﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using JohnDLees.Models;
using System.IO;
using System.Linq;

namespace JohnDLees.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			var indexViewModel = new IndexViewModel();

			var skills = GetSkillsFromCsv().OrderBy(m => m.Type).ThenByDescending(m => m.Level).ToList();

			indexViewModel.Skills = skills;

			return View(indexViewModel);
		}


		private static List<Skill> GetSkillsFromCsv()
        {
            try
            {
                using (var reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + @"\Content\skills.csv"))
                {
                    var skills = new List<Skill>();
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(',');

                        int.TryParse(values[2], out var rating);

                        skills.Add(new Skill
                        {
                            Level = rating,
                            Name = values[1],
                            Type = values[0]
                        });
                    }

                    return skills;
                }
            }
            catch(FileNotFoundException)
            {
                //The file could not be found, show blank
                return new List<Skill>();
            }
		}

		[HttpGet]
		public ActionResult DownloadCv()
		{
            var filename = AppDomain.CurrentDomain.BaseDirectory + "/Content/JohnDLeesCV.pdf";

			return File(filename, "application/pdf", "JohnDLees-CV.pdf");
		}



	}
}