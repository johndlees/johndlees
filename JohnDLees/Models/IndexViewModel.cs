﻿using System.Collections.Generic;

namespace JohnDLees.Models
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			Skills = new List<Skill>();
		}

		public List<Skill> Skills { get; set; }
	}
}