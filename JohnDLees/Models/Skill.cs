﻿namespace JohnDLees.Models
{
	public class Skill
	{
		public string Type { get; set; }

		public string Name { get; set; }

		public int Level { get; set; }

	}
}