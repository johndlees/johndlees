﻿using System;
using System.Diagnostics;
using System.IO;
using JohnDLees.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace JohnDLees.Web.Controllers
{
    public class HomeController : Controller
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ViewData["HeaderLinkOrElement"] = "#";
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult DownloadCv()
        {
            var path = Path.Combine(AppContext.BaseDirectory, @"wwwroot/JohnDLeesCV.pdf");
       
            var stream = new FileStream(path, FileMode.Open);
                   
            return File(stream, "application/pdf", "JohnDLees-CV.pdf");
        }

        public IActionResult Privacy()
        {
            ViewData["HeaderLinkOrElement"] = "/";
            
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        public IActionResult About()
        {
            return RedirectToAction("Index", "Home", "about");
        }

        public IActionResult Software()
        {
            return RedirectToAction("Index", "Home", "software");
        }

        public IActionResult Camera()
        {
            return RedirectToAction("Index", "Home", "camera");   
        }

        public IActionResult Hobbies()
        {
            return RedirectToAction("Index", "Home", "hobbies");   
        }

        public IActionResult Contact()
        {
            ViewData["HeaderLinkOrElement"] = "/";
            
            return View();
        }
    }
}