﻿(function (navbar, $, undefined) {
	"use strict";

	$(document).ready(function () {
		navbar.slowScroll();
		navbar.currentShownDiv();
	});

	var paddingOffset = 80;

	//This adds the class "responsive" to the navigation bar
	navbar.addOrRemoveResponsiveClass = function() {
		var x = document.getElementById("jl-navbar");
		if (x.className === "topnav align-centre") {
			x.className += " responsive";
		} else {
			x.className = "topnav align-centre";
			$("header").className = "empty";
		}
	};

	//This scrolls to links that are on the page slowly
	var scrollTime = 300;
	navbar.slowScroll = function() {
		$("a[href*='#']").on("click",
			function(e) {
				e.preventDefault();

				if ($($.attr(this, "href")).length > 0) {
					$("html, body").animate({
							scrollTop: $($.attr(this, "href")).offset().top - paddingOffset
						},
						scrollTime);

					// Update URL
					window.history.replaceState("", document.title, window.location.href.replace(location.hash, "") + this.hash);
				}
				return false;
			});
	};

	var currentDiv = "";
	var currentScroll = 0;

	navbar.currentShownDiv = function () {
		$(window).scroll(function () {

			var topOfTheWindowOffset = $(window).scrollTop() + paddingOffset + 1;

			if (currentScroll === topOfTheWindowOffset) {
				return;
			}

			var navbarArray = [];

			$("#navbar li > a").each(function () {
				var itemLink = $.attr(this, "href");
				itemLink = itemLink.substring(1, itemLink.length);
				navbarArray.push(itemLink);
			});

			navbarArray.reverse();

			$.each(navbarArray, function (index, value) {
				var linkName = value;

				if ($("div#" + linkName).length > 0) {
			
					if (topOfTheWindowOffset >= $("div#" + linkName).offset().top) {
						
						if (currentDiv !== linkName) {
							
							currentDiv = linkName;
							history.pushState(null, null, "#" + linkName);
						}

						return false;
					}
				}
			});

			return false;
		});
	};

}(window.navbar = window.navbar || {}, jQuery));

