using OpenQA.Selenium;
using Xunit;

namespace JohnDLees.Web.Test.IntegrationTests
{
    [Trait("Category", "Integration Tests")]
    public class NavigationTest : SeleniumTestBase, IClassFixture<LocalServerFactory<Startup>>
    {
        private readonly LocalServerFactory<Startup> _server;

        public NavigationTest(LocalServerFactory<Startup> server)
        {
            _server = server;
            _ = _server.CreateClient(); // this call is needed to change state of server
        }

        [Fact]
        public void HomePageLoad()
        {
            Driver.Navigate().GoToUrl($"{_server.RootUri}");

            Assert.Equal("https://localhost:5001", _server.RootUri);
            Assert.Equal("John D Lees", Driver.Title);
        }

        [Theory]
        [InlineData("About")]
        [InlineData("Software")]
        [InlineData("Camera")]
        [InlineData("Hobbies")]
        public void HomePageNavbarClick_NavigatesSamePage(string linkText)
        {
            Driver.Navigate().GoToUrl($"{_server.RootUri}");

            var navbarLink = Driver.FindElement(By.LinkText(linkText));
            navbarLink.Click();

            Assert.Equal($"{_server.RootUri}/#{linkText.ToLower()}", Driver.Url);
        }

        [Theory]
        [InlineData("Contact", "About")]
        [InlineData("Contact", "Software")]
        [InlineData("Contact", "Camera")]
        [InlineData("Contact", "Hobbies")]
        [InlineData("Privacy", "About")]
        [InlineData("Privacy", "Software")]
        [InlineData("Privacy", "Camera")]
        [InlineData("Privacy", "Hobbies")]
        public void NavbarClick_NavigateAway(string firstLinkText, string secondLinkText)
        {
            Driver.Navigate().GoToUrl($"{_server.RootUri}");

            var navbarLink = Driver.FindElement(By.LinkText(firstLinkText));
            navbarLink.Click();

            Assert.Contains(firstLinkText, Driver.Url);

            HomePageNavbarClick_NavigatesSamePage(secondLinkText);
        }
    }
}