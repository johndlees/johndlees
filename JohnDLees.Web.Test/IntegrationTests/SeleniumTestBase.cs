using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Size = System.Drawing.Size;

namespace JohnDLees.Web.Test.IntegrationTests
{
    public abstract class SeleniumTestBase : IDisposable
    {
        protected readonly IWebDriver Driver;
        private const string ChromeDriverEnvironmentVariableName = "ChromeWebDriver";
        protected SeleniumTestBase()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArgument("--headless");
            chromeOptions.AddArgument("--ignore-certificate-errors");

            // chrome driver is sensitive to chrome browser version, CI build should provide the path to driver
            // for Azure DevOps it's described here for example: https://github.com/actions/virtual-environments/blob/master/images/win/Windows2019-Readme.md
            var chromeDriverLocation = string.IsNullOrEmpty(Environment.GetEnvironmentVariable(ChromeDriverEnvironmentVariableName)) ?
                System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) :
                Environment.GetEnvironmentVariable(ChromeDriverEnvironmentVariableName);
            
            Driver = new ChromeDriver(chromeDriverLocation, chromeOptions);
            Driver.Manage().Window.Size = new Size(1500, 500);
        }
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Driver?.Dispose();
            }
        }
    }
}