using JohnDLees.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace JohnDLees.Web.Test.Controllers
{
    [Trait("Category", "Unit Tests")]
    public class HomeControllerTest
    {
        [Fact]
        public void Index()
        {
            var homeController = new HomeController();
            homeController.OnActionExecuting(null); //Used to test the ViewData

            var result = homeController.Index();

            var viewResult = Assert.IsType<ViewResult>(result);
            
            var headerLinkOrElement = Assert.Single(viewResult.ViewData);
            Assert.Equal("HeaderLinkOrElement", headerLinkOrElement.Key);
            Assert.Equal("#", headerLinkOrElement.Value);
        }

        [Fact]
        public void DownloadCv()
        {
            var homeController = new HomeController();
            var result = homeController.DownloadCv();

            var viewResult = Assert.IsType<FileStreamResult>(result);
            Assert.Equal("application/pdf", viewResult.ContentType);
            Assert.Equal("JohnDLees-CV.pdf", viewResult.FileDownloadName);
        }

        [Fact]
        public void About()
        {
            var homeController = new HomeController();
            var result = homeController.About();
            
            var actionResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("about", actionResult.Fragment);
        }

        [Fact]
        public void Software()
        {
            var homeController = new HomeController();
            var result = homeController.Software();
            
            var actionResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("software", actionResult.Fragment);
        }

        [Fact]
        public void Camera()
        {
            var homeController = new HomeController();
            var result = homeController.Camera();
            
            var actionResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("camera", actionResult.Fragment);
        }
        
        [Fact]
        public void Hobbies()
        {
            var homeController = new HomeController();
            var result = homeController.Hobbies();
            
            var actionResult = Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("hobbies", actionResult.Fragment);
        }

        [Fact]
        public void Contact()
        {
            var controller = new HomeController();
            var result = controller.Contact();

            var viewResult = Assert.IsType<ViewResult>(result);
            
            var headerLinkOrElement = Assert.Single(viewResult.ViewData);
            Assert.Equal("HeaderLinkOrElement", headerLinkOrElement.Key);
            Assert.Equal("/", headerLinkOrElement.Value);
        }

        [Fact]
        public void Privacy()
        {
            var controller = new HomeController();
            var result = controller.Privacy();

            var viewResult = Assert.IsType<ViewResult>(result);
            
            var headerLinkOrElement = Assert.Single(viewResult.ViewData);
            Assert.Equal("HeaderLinkOrElement", headerLinkOrElement.Key);
            Assert.Equal("/", headerLinkOrElement.Value);
        }
    }
}