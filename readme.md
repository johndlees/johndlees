# John D Lees Personal Site

This is the current repo for https://www.johndlees.com/

It is a personal site that lists my current cv, relevant programming skills and some projects I have been working on recently.
It has also been expanded to include to have a place to post about some of my interests and other things
I have been working on, outside of Software. 

It has been upgraded from .Net Framework and now uses dotnet Core 3.1 because I love the testability
and development speed Core offers.
